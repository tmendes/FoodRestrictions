# Food Restrictions

#### Basic information

Food Restriction is a mobile application to help you while travelling abroad. It helps you to show people some or all you food restrictions like allergies or food you don't eat by choice whenever you are ordering your food at the streets or in a restaurant.

#### What is done

  * Graphical Food Restrictions by icons
  * Text Food Restrictions

#### Screenshots

<img src="assets/images/howto.png" width="30%">
<img src="assets/images/config.png" width="30%">
<img src="assets/images/menu.png" width="30%">
<img src="assets/images/main.png" width="30%">
<img src="assets/images/text.png" width="30%">

#### Links

[![The Food Restriction App for Android on F-Droid](https://f-droid.org/wiki/images/c/c4/F-Droid-button_available-on.png)](https://f-droid.org/packages/br.com.frs.foodrestrictions/)
[![The Food Restriction App for Android on Google PlayStore](https://developer.android.com/images/brand/en_app_rgb_wo_60.png)](https://play.google.com/store/apps/details?id=br.com.frs.foodrestrictions)

#### Contact

For any question, bug report, suggestion please drop us a line

#### Translate

Send us a Message

#### The Food Restriction Project Team

##### Project Manager

Thiago Ribeiro Mendes

##### Developers

Thiago Ribeiro Mendes

##### Icons Designers

Marcelo Gomide Torres

# License

    Copyright 2015-2016 The Food Restriction Project Team

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3
    of the License, or (at your option) any later version.

    https://www.gnu.org/licenses/gpl-3.0.html

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
